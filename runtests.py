#!/usr/bin/env python
# coding:utf8
import os
import sys


if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'example.settings'
    from django.conf import settings
    from django.test.utils import get_runner
    TestRunner = get_runner(settings)
    test_runner = TestRunner(verbosity=2)
    arguments = sys.argv[1:] or ["tests"]
    failures = test_runner.run_tests(arguments)
    sys.exit(bool(failures))
