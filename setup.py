# coding: utf8
import os
from setuptools import setup, find_packages


def read_file(name):
    with open(os.path.join(os.path.dirname(__file__), name)) as fd:
        return fd.read()


description = (
    u'Sinops (signed operations) - пакет для создания в django приложении '
    u'представлений, допускающих исполнение, только при наличии специального '
    u'документа подписанного с использованием ЭЦП.'
)


setup(
    name='sinops',
    version='0.1.18',
    packages=find_packages(exclude=('tests', 'example')),
    include_package_data=True,
    author='BARS Group',
    url='https://bitbucket.org/hfarhad/sinops',
    description=description,
    install_requires=read_file("requirements.txt").splitlines(),
    classifiers=[
         'Intended Audience :: Developers',
         'Natural Language :: Russian',
         'Natural Language :: English',
         'Operating System :: OS Independent',
         'Programming Language :: Python',
         'Development Status :: 4 - Beta',
    ],
)
