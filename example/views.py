# coding: utf8
import datetime
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.conf.urls import patterns

from sinops.operation import SignedOperation, StatefulSignedOperation
from sinops.models import OperationType
from sinops.utils import sign_required
from .utils import (
    FakeSuccessVerifySO, FakeSuccessVerifySSO
)

TEST_MESSAGE = u'Тестовое сообщение - Ура Python!'

NON_EXISTING_OPERATION_ID = 314159265


def _get_success(request):
    return HttpResponse("success!")


def _get_time_msg():
    return u'{0} (forming-time: {1}'.format(
        TEST_MESSAGE, datetime.datetime.now()
    )

# ------------------

@sign_required(SignedOperation(TEST_MESSAGE))
def so_fixmsg_view(request):
    return _get_success(request)


@sign_required(lambda r: SignedOperation(_get_time_msg()))
def so_dynmsg_view(request):
    return _get_success(request)


@sign_required(FakeSuccessVerifySO(TEST_MESSAGE))
def so_fake_success_fixmsg_view(request):
    return _get_success(request)


@sign_required(FakeSuccessVerifySO(TEST_MESSAGE))
def so_fake_success_raising_fixmsg_view(request):
    raise AssertionError(u'')

# ------------------

@sign_required(StatefulSignedOperation(TEST_MESSAGE))
def sso_fixmsg_view(request):
    return _get_success(request)


@sign_required(FakeSuccessVerifySSO(TEST_MESSAGE))
def sso_fake_success_fixmsg_view(request):
    return _get_success(request)


def list_examples(request):
    return render_to_response('example/list-examples.html', {
        'example_category': 'Signed Operations Example',
        'examples_list': [{
            'name': v.func_name,
            'url': reverse(v),
        } for v in _collect_views()],
    })


def _collect_views():
    return [obj for key, obj in globals().iteritems()
            if key.endswith('_view') and callable(obj)]

urlpatterns = patterns(
    '',
    (r'^$', list_examples),
    *[(r'^%s$' % v.func_name, v) for v in _collect_views()]
)
