# coding:utf8
from sinops.verification.fake import AlwaysSuccessVerifier
from sinops.operation import SignedOperation, StatefulSignedOperation


class FakeSuccessVerifySO(SignedOperation):

    verifier_classes = [AlwaysSuccessVerifier]


class FakeSuccessVerifySSO(StatefulSignedOperation):

    verifier_classes = [AlwaysSuccessVerifier]
