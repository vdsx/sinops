# coding: utf8
from django.shortcuts import render_to_response
from sinops.interface import ClientInterfaceBase


class SimpleTestInterface(ClientInterfaceBase):

    ASK_SIGNATURE = 'ASK_SIGNATURE'
    OPERATION_COMPLETED = 'OPERATION_COMPLETED'
    OPERATION_ERROR = 'OPERATION_ERROR'
    VERIFY_ERROR = 'VERIFY_ERROR'

    def ask_signature_result(self, request, operation):
        return render_to_response('example/simple-test-interface.html', {
            'result_type': self.ASK_SIGNATURE,
            'result_value': operation.get_content_to_show,
            'content_to_sign': operation.get_content_to_show(),
            'action_url': request.path,
            'signature_param_name': '_signed_message',
        })

    def operation_completed_result(self, request, operation, value):
        return render_to_response('example/simple-test-interface.html', {
            'result_type': self.OPERATION_COMPLETED,
            'result_value': value,
        })

    def operation_error_result(self, request, operation, err):
        return render_to_response('example/simple-test-interface.html', {
            'result_type': self.OPERATION_ERROR,
            'result_value': unicode(err),
        })

    def verify_error_result(self, request, operation, err):
        return render_to_response('example/simple-test-interface.html', {
            'result_type': self.VERIFY_ERROR,
            'result_value': unicode(err),
        })
