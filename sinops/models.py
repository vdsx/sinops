# coding:utf8
u"""Модуль моделей пакета."""

from django.db import models


class Operation(models.Model):

    u"""
    Модель лога совершенных операций.

    Данная модель содержит основные данные по совершенным операциям.
    """

    operation_type = models.ForeignKey(
        'OperationType', verbose_name=u'тип операции')

    signed_message = models.TextField(
        u'Подписанное сообщение')

    detached_content = models.TextField(
        u'Отделенные данные подписи', blank=True)

    verbose_content = models.TextField(
        u'Сообщение подписи')

    signing_time = models.DateTimeField(
        u'Дата и время подписания сообщения', null=True
    )

    certificate = models.ForeignKey(
        'Certificate', verbose_name=u'Сертификат'
    )

    request_path = models.CharField(u'Урл действия', max_length=2000)

    created = models.DateTimeField(
        auto_now_add=True, db_index=True, verbose_name=u'Дата создания'
    )

    modified = models.DateTimeField(
        auto_now=True, db_index=True, verbose_name=u'Дата изменения'
    )

    class Meta:
        verbose_name = u'Операция'


class OperationType(models.Model):

    u"""Тип операции."""

    DEFAULT_OP_TYPE_ID = 1

    code = models.CharField(
        u'Код', max_length=100, unique=True, db_index=True)
    description = models.CharField(u'Описание', max_length=500)

    class Meta:
        verbose_name = u'Тип операции'


class Certificate(models.Model):

    u"""
    Модель данных сертификата открытого ключа, с помощью которого
    подписывается документ любой операции.
    """

    content = models.TextField(
        u'Данные сертификата',
        editable=False,
    )
    sha1hash = models.CharField(
        u'SHA1 отпечаток',
        max_length=40, db_index=True, unique=True,
    )
    subject_name = models.TextField(
        u'Владелец',
        editable=False,
        db_index=True,
    )
    issuer_name = models.TextField(
        u'Эмитент',
        editable=False,
        db_index=True
    )
    serial_number = models.CharField(
        u'Серийный номер', max_length=1024
    )
    validity_begin = models.DateTimeField(
        u'Не действителен до',
        editable=False,
        db_index=True
    )
    validity_end = models.DateTimeField(
        u'Не действителен после',
        editable=False,
        db_index=True
    )

    def __unicode__(self):
        return self.sha1hash

    class Meta:
        verbose_name = u'Сертификат'
