# coding: utf8

from functools import wraps
from django.http import HttpRequest

from sinops.operation import SignedOperationBase
from sinops import exceptions
from sinops.settings import api_settings


def _find_request_instance(*args, **kwargs):

    if isinstance(kwargs.get('request'), HttpRequest):
        return kwargs['request']

    for arg in args:
        if isinstance(arg, HttpRequest):
            return arg

    for arg in kwargs.itervalues():
        if isinstance(arg, HttpRequest):
            return arg


def sign_required(signed_operation):
    u"""
    Декоратор представления, связывающий его с операцией `signed_operation`.

    :param signed_operation: Экземпляр "operation" класса либо callable его
            возвращающий. Сallable будет вызван с теми же аргументами что и
            декорируемое представление.

    .. note::
        Среди аргументов декорируемого представления, должен быть `request`.
    """
    def decorator(fn):

        if api_settings.DISABLE_DECORATORS:
            return fn

        @wraps(fn)
        def wrapper(*args, **kwargs):

            if isinstance(signed_operation, SignedOperationBase):
                operation = signed_operation
            else:
                operation = signed_operation(*args, **kwargs)
                if operation is None:
                    return fn(*args, **kwargs)

            request = _find_request_instance(*args, **kwargs)
            assert isinstance(request, HttpRequest)
            assert isinstance(operation, SignedOperationBase)

            operation.initialize(request)
            interface = operation.get_client_interface()

            try:
                operation.verify()
            except exceptions.SignatureDoesNotSupplied:
                ret = interface.ask_signature_result(request, operation)
            except exceptions.VerificationError as err:
                ret = interface.verify_error_result(request, operation, err)
                if ret is None:
                    raise
            else:
                try:
                    value = fn(*args, **kwargs)
                except Exception as err:
                    ret = interface.operation_error_result(
                        request, operation, err)
                    if ret is None:
                        raise
                else:
                    operation.save()
                    ret = interface.operation_completed_result(
                        request, operation, value,
                    )
            return ret

        return wrapper

    return decorator


class SignHandler(object):

    u"""Хандлер операции. Используется в декораторе with_sign_handler."""

    def __init__(self, so_getter, request):
        self._so_getter = so_getter
        self._request = request
        self._operation = None
        self._sign_required = self._saved = self._verify_succeeded = False

    def is_sign_required(self):
        return self._sign_required

    @property
    def operation(self):
        return self._operation

    def require_signature(self, *so_args, **so_kwargs):

        if api_settings.DISABLE_DECORATORS:
            return

        assert not self._sign_required, u'Sing already requred.'
        self._sign_required = True

        self._operation = self._so_getter(*so_args, **so_kwargs)
        self._operation.initialize(self._request)

        self._operation.verify()
        self._verify_succeeded = True

    def save(self, *args, **kwargs):
        assert self._sign_required, u'Signing does not requred.'
        assert self._verify_succeeded, u'Cannot save invalid operation.'
        assert not self._saved, u'Operation already saved.'

        self._operation.save(*args, **kwargs)
        self._saved = True


class with_sign_handler(object):

    u"""
    Декоратор представления с управляющим handler'ом.

    Декоратор добавляет в декорируемое представление дополнительный
    аргумент `so_handler` - хандлер операции.
    В случае если представление требует подписания, необходимо вызвать
    метод хандлера .require_signature:

    .. code::

        @with_sign_handler(SignedOperation)
        def myview(request, sign_handler)
            if my_condition():
                sign_handler.require_signature(u'message')
            do_something()
            return result

    """

    def __init__(self, so_getter, auto_save=True, arg_name='sign_handler'):
        u"""
        Инициализация декоратор класса.

        :param so_getter: Сallable возвращающий "operation" класс.
        :param bool auto_save: Нужно ли автоматически сохранять операцию.
        :param str arg_name: Имя хандлер аргумента.
        """
        self.so_getter = so_getter
        self.arg_name = arg_name
        self.auto_save = auto_save
        self.fn = None

    def __call__(self, fn):
        self.fn = fn

        @wraps(fn)
        def wrapper(*args, **kwargs):

            request = _find_request_instance(*args, **kwargs)
            sign_handler = SignHandler(self.so_getter, request)

            assert self.arg_name not in kwargs
            kwargs[self.arg_name] = sign_handler

            try:
                fn_result = fn(*args, **kwargs)
            except exceptions.SignatureDoesNotSupplied:
                interface = sign_handler.operation.get_client_interface()
                ret = interface.ask_signature_result(
                    request, sign_handler.operation)
            except exceptions.VerificationError as err:
                interface = sign_handler.operation.get_client_interface()
                ret = interface.verify_error_result(
                    request, sign_handler.operation, err)
                if ret is None:
                    raise
            except Exception as err:
                if sign_handler.is_sign_required():
                    interface = sign_handler.operation.get_client_interface()
                    ret = interface.operation_error_result(
                        request, sign_handler.operation, err)
                    if ret is None:
                        raise
                else:
                    raise
            else:
                if sign_handler.is_sign_required():
                    if self.auto_save and not sign_handler._saved:
                        sign_handler.save()

                    interface = sign_handler.operation.get_client_interface()
                    ret = interface.operation_completed_result(
                        request, sign_handler.operation, fn_result,
                    )
                else:
                    ret = fn_result

            return ret

        return wrapper
