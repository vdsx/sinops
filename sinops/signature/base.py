# coding: utf8


class Signature(object):

    u"""
    Базовый класс, определяющий подписанное с помощью ЭЦП сообщение.

    Наследники должны содержать методы для
    удобного доступа к атрибутам ЭЦП, таким как
    сертификат подписи, подписанные данные, штампы времени и т.д.
    """

    code = None
    u"""
    Значение определяющее тип используемой подписи.
    """

    description = u''
    u"""
    Описание данного вида подписи.
    """

    allow_detached = True
    u"""
    Может ли подпись быть отделенной.
    """

    def __init__(self, data, detached_content=None, options=None):
        self._data = data
        self._detached_content = detached_content
        self._options = options or {}

    @property
    def data(self):
        u"""Данные подписи в каноническом для данного формата представлении."""
        return self._data

    @property
    def origin_content(self):
        u"""
        Строка данных которая была подписана с помощью ЭЦП.
        Должно возвращать значение даже в случае если была использована
        отделенная подпись.
        """
        raise NotImplementedError

    @property
    def verbose_content(self):
        u"""
        Строка текстовых данных которая описывает выполняемое
        действие в понятном для человека виде.
        """
        raise NotImplementedError

    @property
    def signing_time(self):
        u"""Атрибут времени подписания сообщения."""
        raise NotImplementedError

    def get_certificate(self):
        u"""Получить сертификат подписи."""
        raise NotImplementedError
