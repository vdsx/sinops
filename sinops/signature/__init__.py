# coding: utf8

from .base import Signature

# Стандартизированных идентификаторов подписи нет, поэтому
# будем использовать свои.
CADES_BES = 'CADES_BES'
CADES_X_LONG_TYPE_1 = 'CADES_X_LONG_TYPE_1'
