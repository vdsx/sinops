# coding:utf8
from django.utils.functional import cached_property
from pyasn1.error import PyAsn1Error
from pyasn1.codec.der import decoder

from sinops.settings import api_settings
from sinops.signature import CADES_X_LONG_TYPE_1, CADES_BES
from sinops import exceptions
from sinops.signature.base import Signature
from sinops.signature.cert import X509Certificate

from . import parser


class CAdESBES(Signature):

    u"""Формат подписи CAdES-X BES."""

    code = CADES_BES
    description = (
        u'Усовершенствованный формат электронной подписи CAdES BES'
    )
    allow_detached = False
    is_detached = False

    def __init__(self, data, detached_content=None, options=None):
        super(CAdESBES, self).__init__(data, detached_content, options)
        self._content_encoding = (options or {}).get(
            'content_encoding',
            api_settings.CADES_DEFAULT_CONTENT_ENCODING
        )
        self._cadesmsg = self._get_cadesmsg()

    def _get_cadesmsg(self):
        try:
            _cadesmsg = decoder.decode(
                self.data, asn1Spec=parser.CAdESMessage())[0]
        except Exception as err:
            raise exceptions.InvalidSignature(unicode(err))
        return _cadesmsg

    @cached_property
    def data(self):
        return self._data.decode('base64')

    @cached_property
    def origin_content(self):
        return self._cadesmsg.get_content()

    @cached_property
    def verbose_content(self):
        return self.origin_content.decode(self._content_encoding)

    @cached_property
    def signing_time(self):
        u"""Возвращает время подписания в тамзоне UTC."""
        utc_time = self._cadesmsg.get_signing_time()
        assert utc_time.tzname() == 'UTC'
        return utc_time

    def get_certificate(self):
        cert = self._cadesmsg.get_signature_certificate()
        return X509Certificate(cert)


class CAdESXLT1(CAdESBES):

    u"""Формат подписи CAdES-X Long Type 1."""

    code = CADES_X_LONG_TYPE_1
    description = (
        u'Усовершенствованный формат электронной подписи CAdES-X Long Type 1'
    )
    allow_detached = False
    is_detached = False
