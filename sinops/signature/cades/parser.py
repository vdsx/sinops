# coding: utf8
u"""
Модуль реализует парсинг подписанного сообщения формата CAdES.

.. note::
    Парсятся не все эллементы подписи, а только необходимые пакету.
    Модуль был реализован не углубляясь в спецификацию формата CAdES.
    В будущем необходимо либо доработать данный модуль либо заменить
    на сторонний.
"""

from pyasn1.type import univ, namedtype, tag
from pyasn1.codec.der.decoder import decode
from pyasn1_modules import rfc2315, rfc2459

from sinops.signature.asn1utils import extract_time


__all__ = ['CAdESMessage']


def _cctag(n):
    return tag.Tag(tag.tagClassContext, tag.tagFormatConstructed, n)


def _find_by_oid(component, oid, spec=None):
    for item in component:
        if str(item['oid']) == oid:
            return decode(item['value'], asn1Spec=spec)[0]


class AttributeTypeAndValue(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('oid', univ.ObjectIdentifier()),
        namedtype.NamedType('value', univ.Any())
    )


class AuthenticatedAttributes(univ.SetOf):

    componentType = AttributeTypeAndValue()

    _OID_SIGNED_TIME = '1.2.840.113549.1.9.5'

    def get_signing_time(self):
        comp = _find_by_oid(self, self._OID_SIGNED_TIME)
        return extract_time(comp[0])


class InfoContainer(univ.SequenceOf):

    _OID_CERT_VALUES = '1.2.840.113549.1.9.16.2.23'

    componentType = AttributeTypeAndValue()

    def get_certificates_components(self):
        comp = _find_by_oid(
            self, self._OID_CERT_VALUES,
            univ.SetOf(univ.SequenceOf(univ.Any()))
        )
        return comp[0]


class InfoSeq(univ.Sequence):

    componentType = namedtype.NamedTypes(
        namedtype.NamedType(
            'version',
            univ.Integer()
        ),
        namedtype.NamedType(
            'issuerAndSerialNumber',
            rfc2315.IssuerAndSerialNumber()
        ),
        namedtype.NamedType(
            'digestAlgorithm',
            univ.Sequence()
        ),
        namedtype.OptionalNamedType(
            'authenticatedAttributes',
            AuthenticatedAttributes().subtype(implicitTag=_cctag(0))
        ),
        namedtype.NamedType(
            'digestEncryptionAlgorithm',
            univ.Sequence(),
        ),
        namedtype.NamedType(
            'encryptedDigest',
            univ.OctetString()
        ),
        namedtype.OptionalNamedType(
            'info',
            InfoContainer().subtype(implicitTag=_cctag(1))
        ),
    )


class ContentInfo(univ.Sequence):

    componentType = namedtype.NamedTypes(
        namedtype.NamedType('oid', univ.ObjectIdentifier()),
        namedtype.OptionalNamedType(
            'content', univ.OctetString().subtype(explicitTag=_cctag(0)))
    )


class SignedData(univ.Sequence):

    componentType = namedtype.NamedTypes(
        namedtype.NamedType(
            'version', rfc2315.Version()),
        namedtype.NamedType(
            'digestAlgorithms', rfc2315.DigestAlgorithmIdentifiers()),
        namedtype.NamedType(
            'contentInfo', ContentInfo()
        ),
        namedtype.OptionalNamedType(
            'certificates',
            univ.SetOf(univ.Any()).subtype(implicitTag=_cctag(0))
        ),
        namedtype.OptionalNamedType(
            'crls',
            rfc2315.CertificateRevocationLists().subtype(
                implicitTag=_cctag(1))
        ),
        namedtype.NamedType('set', univ.SetOf(InfoSeq())),
    )


class CAdESMessage(univ.Sequence):

    u"""Класс pyasn1 для сообщения подписанного в формате CAdES."""

    componentType = namedtype.NamedTypes(
        namedtype.NamedType('oid', rfc2315.signedData),
        namedtype.NamedType('signedData', SignedData().subtype(
            explicitTag=_cctag(0))),
    )

    def get_content(self):
        comp = self['signedData']['contentInfo']['content']
        if comp.hasValue():
            return comp.asOctets()

    def get_signature_certificate(self):
        certs_data = []
        comp = self.get_info_comp()
        cert_serial = comp['issuerAndSerialNumber']['serialNumber']

        certs_data.extend(self['signedData']['certificates'] or [])
        if comp['info']:
            certs_data.extend(
                comp['info'].get_certificates_components() or []
            )

        for cert_data in certs_data:
            cert = decode(cert_data, asn1Spec=rfc2459.Certificate())[0]
            serial = cert['tbsCertificate']['serialNumber']
            if serial == cert_serial:
                return cert_data.asOctets()

    def get_signing_time(self):
        comp = self.get_info_comp()
        return comp['authenticatedAttributes'].get_signing_time()

    def get_info_comp(self):
        return self['signedData']['set'][0]
