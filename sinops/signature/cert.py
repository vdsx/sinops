# coding: utf8
import re
import hashlib
from django.utils.functional import cached_property
from pyasn1.codec.der.decoder import decode
from pyasn1.error import PyAsn1Error

from sinops.signature import asn1utils


def _make_property(oid):
    def getter(self):
        return self.attributes.get(oid, [None])[0]

    def setter(self, val):
        if val is not None:
            self.attributes[oid] = [val]
        elif oid in self.attributes:
            del self.attributes[oid]

    return property(getter, setter)


class Name(object):

    u"""
    Класс, представляющий собой имя владельца и эмитента сертификата.
    """

    _oid2name = {
        "2.5.4.3": "CN",
        "2.5.4.6": "C",
        "2.5.4.7": "L",
        "2.5.4.8": "ST",
        "2.5.4.10": "O",
        "2.5.4.11": "OU",
        "1.2.840.113549.1.9.1": "E",
    }

    common_name = _make_property('2.5.4.3')
    country = _make_property('2.5.4.6')
    locality = _make_property('2.5.4.7')
    state_or_province = _make_property('2.5.4.8')
    organization = _make_property('2.5.4.10')
    organization_unit = _make_property('2.5.4.11')
    email = _make_property('1.2.840.113549.1.9.1')

    __rdn_escape_re = re.compile(
        r'((^[# ])|([ ]$)|([\\,+"<>;=/]))', re.UNICODE)

    def __init__(self, attributes):
        self.attributes = {}
        self.attributes.update(attributes)

    def to_distinguished_name(self):
        u"""
        Возвращает строку в "Distinguished Name" формате.
        :rtype: unicode
        """

        def escape(val):
            return self.__rdn_escape_re.subn(ur'\\\1', val)[0]

        prefixed_pairs = []
        for oid, values in self.attributes.iteritems():
            prefix = self._oid2name.get(oid, oid)
            prefixed_pairs.extend((prefix, val) for val in values)

        # отсортируем по приоритету
        order = dict((prefix, idx) for idx, prefix in enumerate(
            ('CN', 'C', 'L', 'ST', 'O', 'OU', 'E'))
        )
        prefixed_pairs.sort(key=lambda x: (order.get(x[0], len(order)), x))

        result = u', '.join(
            u'{0}={1}'.format(prefix, escape(val)) for prefix, val in
            prefixed_pairs if val is not None
        )
        return result

    __unicode__ = to_distinguished_name

    def __str__(self):
        return unicode(self).encode('utf8')


class X509Certificate(object):

    u"""Класс инкапсулирующий работу с X509 сертификатом."""

    __pem_markers = (
        '-----BEGIN CERTIFICATE-----',
        '-----END CERTIFICATE-----',
    )

    def __init__(self, der_content):
        self._der_content = der_content
        try:
            self._cert = decode(
                der_content, asn1Spec=asn1utils.Certificate())[0]
        except PyAsn1Error:
            raise ValueError('Invalid certificate')

    @cached_property
    def version(self):
        return int(self._cert['tbsCertificate']['version']) + 1

    @cached_property
    def serial_number(self):
        return int(self._cert['tbsCertificate']['serialNumber'])

    @cached_property
    def subject(self):
        return Name(self._cert.get_subject_attributes())

    @cached_property
    def issuer(self):
        return Name(self._cert.get_issuer_attributes())

    @cached_property
    def validity_range(self):
        return self._cert.get_validity_range()

    @cached_property
    def signature_value(self):
        val = int(''.join(str(x) for x in self._cert['signatureValue']), 2)
        return '{0:x}'.format(val)

    @cached_property
    def signature_algorithm(self):
        return str(self._cert['signatureAlgorithm']['algorithm'])

    def get_fingerprint(self, alg='sha1'):
        hash_obj = hashlib.new(alg)
        hash_obj.update(self._der_content)
        return hash_obj.hexdigest()

    def as_der(self):
        return self._der_content

    def as_pem(self):
        pem = '\n'.join((
            self.__pem_markers[0],
            self.as_der().encode('base64'),
            self.__pem_markers[1],
        ))
        return pem
