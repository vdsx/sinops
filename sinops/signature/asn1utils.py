# coding: utf8
import pytz
import datetime

from pyasn1.type import univ, char, namedtype, useful
from pyasn1_modules import rfc2459
from pyasn1.codec.der import decoder


OID_NAMES = [
    ("2.5.4.45", "X500UID"),
    ("1.2.840.113549.1.9.1", "email"),
    ("2.5.4.17", "zip"),
    ("2.5.4.9", "street"),
    ("2.5.4.15", "businessCategory"),
    ("2.5.4.5", "serialNumber"),
    ("2.5.4.43", "initials"),
    ("2.5.4.44", "generationQualifier"),
    ("2.5.4.4", "surname"),
    ("2.5.4.42", "givenName"),
    ("2.5.4.12", "title"),
    ("2.5.4.46", "dnQualifier"),
    ("2.5.4.65", "pseudonym"),
    ("0.9.2342.19200300.100.1.25", "DC"),
]


def extract_time(component):
    u"""Получение времени из pyasn1 компонента."""
    if isinstance(component, useful.UTCTime):
        time = datetime.datetime.strptime(
            str(component), '%y%m%d%H%M%SZ'
        ).replace(tzinfo=pytz.utc)
    elif isinstance(component, useful.GeneralizedTime):
        time = datetime.datetime.strptime(
            str(component), '%Y%m%d%H%M%SZ'
        )
    else:
        raise ValueError(
            'component must be GeneralizedTime or UTCTime instance.')
    return time


class X520UnivString(univ.Choice):

    u"""Универсальное значение для текстовых компонентов."""

    componentType = namedtype.NamedTypes(
        namedtype.NamedType('teletexString', char.TeletexString()),
        namedtype.NamedType('printableString', char.PrintableString()),
        namedtype.NamedType('universalString', char.UniversalString()),
        namedtype.NamedType('utf8String', char.UTF8String()),
        namedtype.NamedType('bmpString', char.BMPString()),
        namedtype.NamedType('ia5String', char.IA5String()),
        namedtype.NamedType('numericString', char.NumericString()),
        namedtype.NamedType('generalString', char.GeneralString()),
    )


class Certificate(rfc2459.Certificate):

    u"""Расширение сертификата методами получения данных."""

    def get_subject_attributes(self):
        return self._extract_attributes(
            self['tbsCertificate']['subject'].getComponent()
        )

    def get_issuer_attributes(self):
        return self._extract_attributes(
            self['tbsCertificate']['issuer'].getComponent()
        )

    @classmethod
    def _extract_attributes(cls, items):

        attributes = {}

        def extract_value(der_data):
            comp = decoder.decode(der_data, asn1Spec=X520UnivString())[0]
            if isinstance(comp, univ.Choice):
                comp = comp.getComponent()
            if comp.hasValue():
                return unicode(comp)

        for item in items:
            component = item[0]
            oid = str(component['type'])
            attributes.setdefault(oid, []).append(
                extract_value(component['value'])
            )
        return attributes

    def get_validity_range(self):
        validity = self['tbsCertificate']['validity']

        return (
            extract_time(validity['notBefore'][0]),
            extract_time(validity['notAfter'][0]),
        )
