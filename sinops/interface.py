# coding: utf8
u"""Модуль определяет интерфейсы взаимодействия с клиентом."""


class ClientInterfaceBase(object):

    u"""Базовый интерфейс для взаимодействия с клиентом."""

    def ask_signature_result(self, request, operation):
        u"""
        Возвращает результат представления, который должен сообщить
        клиенту информацию о том, что совершаемая им операция
        должна быть подписана с использованием ЭЦП.

        :param request: Объект запроса.
        :param operation: Выполняющаяся операция.
        :type operation: Потомок sinops.operation.SignedOperationBase.
        """
        raise NotImplementedError

    def operation_completed_result(self, request, operation, value):
        u"""
        Возвращает результат представления, информирующий
        клиента о том, что совершаемая им операция успешно выполненна.

        :param request: Объект зпроса.
        :param operation: Выполняющаяся операция.
        :type operation: Потомок sinops.operation.SignedOperationBase.
        :param value: Результат выполнения представления.
        """
        raise NotImplementedError

    def verify_error_result(self, request, operation, err):
        u"""
        Возвращает результат представления, информирующий
        клиента о том, что во время верификации операции произошла ошибка.

        .. note::
            Если метод не знает тип ошибки, то он должен вернуть None.

        :param request: Объект запроса.
        :param operation: Выполняющаяся операция.
        :type operation: Потомок sinops.operation.SignedOperationBase.
        :param err: Отловленная ошибка верификации.
        """
        raise NotImplementedError

    def operation_error_result(self, request, operation, err):
        u"""
        Возвращает результат представления, информирующий
        клиента о том, что операция была успешно верифицированна, однако
        соответствующим представлением было выброшено исключение.

        .. note::
            Если метод не знает тип ошибки, то он должен вернуть None.

        :param request: Объект запроса.
        :param operation: Выполняющаяся операция.
        :type operation: Потомок sinops.operation.SignedOperationBase.
        :param err: Отловленная ошибка выполнения представления.
        """
        raise NotImplementedError
