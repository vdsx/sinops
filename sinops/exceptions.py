# coding:utf8
u"""Общий модуль для всех исключений."""


class VerificationError(Exception):

    u"""
    Базовый класс для всех исключений, которые должны выбрасываться
    при ошибках верификации операций.
    """

    _message = u'Ошибка верификации'

    def __init__(self, message=None):
        if message is not None:
            self._message = message

    def __str__(self):
        return unicode(self)

    def __unicode__(self):
        return unicode(self._message)


class SignatureDoesNotSupplied(VerificationError):

    u"""
    Исключение, которое должно выбрасываться в случаях, когда
    выполняемая операция требует подписания, однако,
    в параметрах запроса отсутствует необходимая ЭЦП.
    """

    _message = u'Запрос не содержит подписи'


class InvalidSignature(VerificationError):

    u"""
    Исключение, которое должно выбрасываться в случаях получения
    от клиента подписи неверного формата.
    """

    _message = u'Некорректный формат подписи'


class InvalidContext(VerificationError):

    u"""
    Исключение, которое должно выбрасываться в случаях получения
    от клиента некорректного контекста операции.
    """

    _message = u'Некорректный контекст подписи'


class VerificationImpossibleError(VerificationError):

    u"""
    Исключение, которое должно выбрасываться в случаях, когда
    ЭЦП невозможно проверить.(Например отсутствует соединение с
    удаленным сервисом проверки)
    """

    _message = u'Невозможно проверить ЭЦП'


class UnsuitableSignatureType(VerificationError):

    u"""
    Должна выбрасываться backend'ом проверки подписи в случае, если
    он не поддерживает переданный ему на проверку формат ЭЦП.
    """

