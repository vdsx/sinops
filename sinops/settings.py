# coding: utf8
u"""
Модуль настроек пакета sinops.

.. note::
    Код портирован с django rest_framework'a с небольшими изменениями.

Пример::

    SINOPS_SETTINGS = {
        'DEFAULT_VERIFIER_CLASSES': (
            'sinops.signature.cades.CAdESXLT1',
        )
    }
"""

from django.conf import settings
from django.utils import importlib


USER_SETTINGS = getattr(settings, 'SINOPS_SETTINGS', None)


DEFAULTS = {
    'DEFAULT_CLIENT_INTERFACE_CLASS': None,
    'DEFAULT_SIGNATURE_CLASSES': [
        'sinops.signature.cades.CAdESBES',
    ],
    'DEFAULT_VERIFIER_CLASSES': [
        'sinops.verification.smev.SMEVVerifier',
    ],
    'DEFAULT_SIGNING_TIME_PRECISION': (-60, 60*4),
    'SMEV_VERIFIER_SERVICE_URL': (
        'http://195.245.214.33:7777/esv/SignatureTool.asmx'),
    'SIGNATURE_PARAM_NAME': '_signed_message',
    'CONTEXT_PARAM_NAME': '_context_data',
    'TSA_SERVICE_ADDRESS': 'http://testca.cryptopro.ru/tsp/tsp.srf',
    'CADES_DEFAULT_CONTENT_ENCODING': 'utf-16-le',
    'DISABLE_DECORATORS': False,
}


# List of settings that may be in string import notation.
IMPORT_STRINGS = [
    'DEFAULT_CLIENT_INTERFACE_CLASS',
    'DEFAULT_VERIFIER_CLASSES',
    'DEFAULT_SIGNATURE_CLASSES'
]


def perform_import(val, setting_name):
    """
    If the given setting is a string import notation,
    then perform the necessary import or imports.
    """
    if isinstance(val, basestring):
        return import_from_string(val, setting_name)
    elif isinstance(val, (list, tuple)):
        return [import_from_string(item, setting_name) for item in val]
    return val


def import_from_string(val, setting_name):
    """
    Attempt to import a class from a string representation.
    """
    try:
        # Nod to tastypie's use of importlib.
        parts = val.split('.')
        module_path, class_name = '.'.join(parts[:-1]), parts[-1]
        module = importlib.import_module(module_path)
        return getattr(module, class_name)
    except ImportError as e:
        msg = "Could not import '%s' for API setting '%s'. %s: %s." % (
            val, setting_name, e.__class__.__name__, e)
        raise ImportError(msg)


class APISettings(object):
    """
    A settings object, that allows API settings to be accessed as properties.
    For example:

        from sinops.settings import api_settings
        print(api_settings.SIGNATURE_PARAM_NAME)

    Any setting with string import paths will be automatically resolved
    and return the class, rather than the string literal.
    """
    def __init__(self, user_settings=None, defaults=None, import_strings=None):
        self.user_settings = user_settings or {}
        self.defaults = defaults or {}
        self.import_strings = import_strings or {}

    def __getattr__(self, attr):
        if attr not in self.defaults:
            raise AttributeError("Invalid API setting: '%s'" % attr)

        try:
            # Check if present in user settings
            val = self.user_settings[attr]
        except KeyError:
            # Fall back to defaults
            val = self.defaults[attr]

        # Coerce import strings into classes
        if val and attr in self.import_strings:
            val = perform_import(val, attr)

        # Cache the result
        setattr(self, attr, val)
        return val


api_settings = APISettings(USER_SETTINGS, DEFAULTS, IMPORT_STRINGS)
