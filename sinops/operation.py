# coding: utf8
u"""
Данный модуль содержит определения "operation" классов.

.. note::
    Под "operation" классом, здесь и далее будет пониматься
    любой потомок класса SignedOperationBase.

Концепция работы с данным пакетом предполагает, что любое
представление, требующее подписания, должно быть связанно с
некоторым экземпляром "operation" класса, инкапсулирующим
всю логику работы с ЭЦП начиная от формирования документа
до верификации подписи и логирования.
"""

import abc
import datetime
import hashlib
import json

from .settings import api_settings
from . import exceptions, models


class SignedOperationBase(object):

    u"""
    Абстрактный, базовый "operation" класс.

    Класс определяет как действие совершаемое некоторым представлением,
    должно быть подписано, проверено и сохранено(залогировано).
    """

    __metaclass__ = abc.ABCMeta

    _initialized = False

    def _assert_initialized(self):
        if not self.initialized:
            raise AssertionError('Operation is not initialized.')

    @property
    def initialized(self):
        u"""Подготовлена ли операция к подписанию или проверке."""
        return self._initialized

    def initialize(self, *args, **kwargs):
        u"""Подготовка операции перед отправкой на подписание или проверкой."""
        self._initialized = True

    @abc.abstractmethod
    def get_client_interface(self):
        u"""
        Возвращает интерфейс взаимодействия с клиентом.

        Данный интерфейс должен определять каким образом нужно
        взаимодействовать с клиентом, для того чтобы выполнить
        подписание, сообщить об статусе операции, ошибках и т.д
        """
        self._assert_initialized()

    @abc.abstractmethod
    def save(self, *args, **kwargs):
        u"""
        Сохранение подписанных данных.

        Метод должен определять как после успешного выполнения операции
        ее нужно залогировать.
        """
        self._assert_initialized()

    @abc.abstractmethod
    def verify(self):
        u"""
        Верификация операции.

        В случае успешной проверки должен возвращать None, иначе
        выбрасывать исключение sinops.exceptions.VerificationError
        (или другое наследуемое от него исключение)
        :returns: None
        :raises: sinops.exceptions.VerificationError
        """
        self._assert_initialized()


class SignedOperation(SignedOperationBase):

    u"""
    Основной "operation" класс модуля.

    При инстанцировании принимает некоторые данные для подписания
    и не обязательный аргумент id типа операции.
    По умолчанию использует классы подписей, интерфейс взаимодействия
    с клиентом и бэкенды верификации ЭЦП задаваемые соответствующими
    настройками пакета.
    """

    verifier_classes = api_settings.DEFAULT_VERIFIER_CLASSES
    u"""
    Классы бэкендов верификации ЭЦП.
    """

    signature_classes = api_settings.DEFAULT_SIGNATURE_CLASSES
    u"""
    Классы подписей, которые могут использоваться для подписания
    данной операции
    """

    client_interface_class = api_settings.DEFAULT_CLIENT_INTERFACE_CLASS
    u"""
    Класс интерфейса взаимодействия с клиентом
    """

    signing_time_precision = api_settings.DEFAULT_SIGNING_TIME_PRECISION
    u"""
    Допустимая разница(погрешность) между моментом начала верификации и
    значением аттрибута времени формирования подписи (если эта информация
    содержится в подписи).
    Задается кортежем минимального и максимально значений разницы.
    """

    operation_type_id = models.OperationType.DEFAULT_OP_TYPE_ID
    u"""
    Тип операции по умолчанию
    """

    _request_path = _context = None
    _signature_data = _cached_signature_obj = None

    def __init__(self, initial_data, operation_type_id=None):
        u"""
        Принимает некоторые данные для подписания и id типа операции.

        :param initial_data: Данные для подписания
        :param int operation_type_id: Ид типа операции
        """
        self.initial_data = initial_data

        if operation_type_id is not None:
            self.operation_type_id = operation_type_id

    def initialize(self, request):
        u"""
        Подготавливает объект операции.

        Получение из request параметров необходимых для
        последующей верификации подписи.
        """
        super(SignedOperation, self).initialize(request)

        self._cached_signature = None
        self._request_path = request.path
        self._signature_data = request.POST.get(
            api_settings.SIGNATURE_PARAM_NAME
        )
        self._context = request.POST.get(
            api_settings.CONTEXT_PARAM_NAME
        )

    @property
    def context(self):
        u"""
        Дополнительный контекст, переданный клиентом.

        :returns: контекст может содержать информацию о типе подписи,
                  кодировке, отделенные данные подписи и.д

        :rtype: dict
        :raises: sinops.exceptions.InvalidContext
        """
        context = {}
        if self._context is not None:
            try:
                context = json.loads(self._context)
                assert isinstance(context, dict)
            except (ValueError, AssertionError):
                raise exceptions.InvalidContext
        return context

    def get_client_interface(self):
        u"""Возвращает интерфейс взаимодействия с клиентом."""
        return self.client_interface_class()

    def get_content_to_show(self):
        u"""
        Получить данные, которые клиент будет видеть при подписании.

        Совпадает с данными для подписания.

        .. note::
            Данный метод добавлен на случай, если подписываемые
            данные будут отличаются от тех, которые нужно показать клиенту.
            Такая необходимость может возникнуть, например, при подписывании
            хеша сообщения.
        """
        return self.get_content_to_sign()

    def get_content_to_sign(self):
        u"""Получить данные, которые будут подписаны."""
        return self.initial_data

    def _get_detached_content(self):
        u"""
        Получить исходные отделенные данные, которые были подписаны.

        .. note::
            Необходимо в случаях использования отделенной подписи, когда
            подписанные данные не содержатся в теле подписи.
        """
        return self.get_content_to_sign()

    def _select_signature_class(self):
        u"""Производит выбор класса подписи."""
        signature_class = None
        code = self.context.get('signature_code')

        if code is not None:
            for sig_cls in self.signature_classes:
                if sig_cls.code == code:
                    signature_class = sig_cls
                    break
            if signature_class is None:
                raise exceptions.InvalidSignature(
                    u'Тип ЭЦП "{0}" - недопустим'.format(code)
                )
        else:
            if len(self.signature_classes) > 1:
                raise exceptions.InvalidContext(
                    u'Параметр контекста signature_code - обязателен.'
                )

            signature_class = self.signature_classes[0]

        return signature_class

    def get_signature(self):
        u"""
        Получить объект подписи.

        :returns: Экземпляр подписи
        :rtype: Потомок sinops.signature.base.Signature
        :raises: sinops.exceptions.SignatureDoesNotSupplied
        :raises: sinops.exceptions.InvalidContext
        """
        if self._signature_data is None:
            raise exceptions.SignatureDoesNotSupplied

        if getattr(self, '_cached_signature_obj', None):
            return self._cached_signature_obj

        signature_class = self._select_signature_class()

        _detached_content = None
        if signature_class.allow_detached:
            _detached_content = self._get_detached_content()

        signature = signature_class(
            self._signature_data,
            _detached_content,
            self.context.get('signature_options')
        )

        self._cached_signature_obj = signature
        return signature

    def verify_signing_time_precision(self):
        u"""
        Проверка, точности времени подписания.

        Выполняет проверку того, что время момента подписания
        и время момента выполнения операции различается незначительно.
        В случае успешного выполнения возвращает None,
        иначе выбрасывает исключение VerificationError.

        :returns: None
        :raises: sinops.exceptions.VerificationError
        """
        if self.signing_time_precision is None:
            return

        signature = self.get_signature()
        signing_time = signature.signing_time

        if signing_time is not None:
            _min, _max = self.signing_time_precision
            sig_time = signature.signing_time.replace(tzinfo=None)
            delta = datetime.datetime.utcnow() - sig_time
            if not (_min <= delta.total_seconds() <= _max):
                raise exceptions.VerificationError(
                    u'Атрибут момента подписания, содержащийся в подписи, '
                    u'имеет большую погрешность. Проверьте точность '
                    u'системного времени.'
                )

    def verify_operation(self):
        u"""
        Проверка, что операция соответствует подписанным данным.

        Выполняет проверку того, что операция действительно соответствует
        сообщению которое было подписано. В случае успешного выполнения
        возвращает None, иначе выбрасывает исключение VerificationError.

        :returns: None
        :raises: sinops.exceptions.VerificationError
        """
        signature = self.get_signature()

        if signature.verbose_content != self.get_content_to_sign():
            raise exceptions.VerificationError(
                u'Подписано некорректное сообщение'
            )

    def verify_signature(self):
        u"""
        Проверка, что подпись действительна.

        Выполняет проверку ЭЦП сообщения. В случае успешного выполнения
        возвращает None, иначе выбрасывает исключение VerificationError.

        :returns: None
        :raises: sinops.exceptions.VerificationError
        """
        signature = self.get_signature()

        # Найдем бэкенды проверки, которые поддерживают тип использованной
        # подписи и инстанцируем их.
        verifiers = [
            verifier() for verifier in self.verifier_classes
            if verifier.is_suitable(signature)
        ]

        error_messages = []
        for verifier in verifiers:
            try:
                verifier.verify(signature)
            except exceptions.VerificationImpossibleError as err:
                # Данный бэкенд не смог проверить подпись.
                # Попробуем выполнить проверку с помощью другого бэкенда.
                error_messages.append(err.message)
                continue
            else:
                # Подпись успешно прошла верификацию.
                # завершаем проверку.
                return

        # Ни один бэкенд не смог проверить подпись. Выбрасываем исключение:
        raise exceptions.VerificationImpossibleError(
            u'Подпись невозможно проверить. {0}'.format(
                u'\n'.join(error_messages))
        )

    def verify(self):
        u"""
        Полная проверка операции.

        В случае успешной проверки возвращается None, иначе выбрасывается
        исключение VerificationError.
        :returns: None
        :raises: sinops.exceptions.VerificationError
        """
        super(SignedOperation, self).verify()
        self.verify_signing_time_precision()
        self.verify_operation()
        self.verify_signature()

    def save(self, *args, **kwargs):
        u"""
        Сохранение операции.

        :raises: django.exceptions.ValidationError
        """
        signature = self.get_signature()
        cert = signature.get_certificate()
        sha1fp = cert.get_fingerprint('sha1')

        try:
            certificate = models.Certificate.objects.get(sha1hash=sha1fp)
        except models.Certificate.DoesNotExist:

            certificate = models.Certificate(
                content=cert.as_der().encode('base64'),
                sha1hash=sha1fp,
                validity_begin=cert.validity_range[0],
                validity_end=cert.validity_range[1],
                serial_number=unicode(cert.serial_number),
                subject_name=unicode(cert.subject),
                issuer_name=unicode(cert.issuer),
            )
            certificate.full_clean()
            certificate.save()

        op = models.Operation(
            operation_type_id=self.operation_type_id,
            signed_message=signature.data.encode('base64'),
            verbose_content=signature.verbose_content,
            signing_time=signature.signing_time,
            request_path=self._request_path,
            certificate=certificate,
        )
        op.full_clean()
        op.save()
        return super(SignedOperation, self).save()


class StatefulSignedOperation(SignedOperation):

    u"""
    Operation класс сохраняющий хеш данных для подписания.

    Основное отличие данного класса от ``sinops.operation.SignedOperation``
    состоит в том, что он обладает измененной логикой верификации
    операции (см. метод ``.verify_operation()``).
    Так как подписанные клиентом данные могут отличаться от данных
    которые предполагаются быть подписанными, но при этом все же быть
    валидными (например, могут отличаются временным маркером содержащимся в
    сообщении), то это может привести к ошибке верификации.
    Для обхода этой проблемы, верификация операции в данном классе
    реализуется не путем сравнения подписанного сообщения и сообщения
    которое планируется быть подписанным, а несколько иначе:

    1. При получении данных для подписания, вычисляется хеш от
       от этих данных и некоторых параметров запроса. Вычисленный хеш
       сохраняется в сессии.

    2. При верификации операции, снова вычисляется хеш, но теперь уже от
       подписанных данных и новых параметров запроса.
       Сравнивается только что вычисленных хеш и хеш сохраненный в сесси.

    Дополнительно, добавлена возможность ограничить время на выполнение
    подписания. см ``StatefulSignedOperation.OPERATION_TIMEOUT``.
    """

    OPERATION_TIMEOUT = 3600
    u"""
    Таймаут выполнения операции. Определяет максимальное время
    между тем как клиенту были отправлены данные для подписания и
    и моментом когда была произведена верификация.
    """

    force_state_update = False
    u"""
    Принудительное обновление состояния, после повторного получения
    данных для подписания.
    """

    _request = None
    _state_saved = False

    def initialize(self, request):
        self._state_saved = False
        self._request = request
        super(StatefulSignedOperation, self).initialize(request)

    def get_content_to_sign(self):
        u"""
        Получить данные, которые будут подписаны.

        .. note::
            После первого обращения к данному методу, состояние
            операции сохраняется в сессии
        """
        content_to_sign = super(
            StatefulSignedOperation, self).get_content_to_sign()
        self._save_state(content_to_sign)
        return content_to_sign

    @classmethod
    def _calculate_request_digest(cls, request, content):
        m = hashlib.sha256()
        excludes = [
            api_settings.CONTEXT_PARAM_NAME,
            api_settings.SIGNATURE_PARAM_NAME,
        ]
        for key, val in sorted(request.REQUEST.iteritems()):
            if key not in excludes:
                m.update(key.encode('utf8'))
                m.update(val.encode('utf8'))

        m.update(request.path.encode('utf8'))
        m.update(content.encode('utf8'))
        return m.hexdigest()

    @classmethod
    def _make_session_key(cls, request):
        return u'sinops_{0}'.format(request.path)

    def _save_state(self, content_to_sign):
        u"""Сохранение состояния в сессии."""
        if self._state_saved and not self.force_state_update:
            return
        session_key = self._make_session_key(self._request)
        digest = self._calculate_request_digest(
            self._request, content_to_sign)
        state = {
            'created': datetime.datetime.now(),
            'digest': digest,
        }
        self._request.session[session_key] = state
        self._state_saved = True

    def verify_operation(self):
        u"""
        Проверка, что операция соответствует подписанным данным.

        :returns: None
        :raises: sinops.exceptions.VerificationError
        """
        signature = self.get_signature()
        content = signature.verbose_content
        digest = self._calculate_request_digest(
            self._request, content)
        session_key = self._make_session_key(self._request)
        state = self._request.session.get(session_key)
        if not state or state['digest'] != digest:
            raise exceptions.VerificationError(
                u'Подписано некорректное сообщение!'
            )

        max_delta = datetime.timedelta(seconds=self.OPERATION_TIMEOUT)
        if (datetime.datetime.now() - state['created']) > max_delta:
            raise exceptions.VerificationError(
                u'Превышено время выполнения операции!'
            )
