# coding: utf8
import os
import requests
from lxml import etree
from django.utils.functional import SimpleLazyObject
from django.template import Template, Context

from sinops.settings import api_settings
from sinops.verification.base import VerifierBase
from sinops.signature import CADES_X_LONG_TYPE_1
from sinops import exceptions


def _get_template():
    filename = os.path.join(os.path.dirname(__file__), 'template.xml')
    with open(filename, 'r') as fp:
        return Template(fp.read())

_template = SimpleLazyObject(_get_template)


class SMEVVerifier(VerifierBase):

    u"""Бэкенд проверки подписи через сервис СМЭВ."""

    _SUITABLE_SIGNATURE_TYPES = [
        CADES_X_LONG_TYPE_1,
    ]

    SUCCESS_CODE = 0

    RESPONSE_CODES = [
        (SUCCESS_CODE, u'Проверка прошла успешно'),
        (1, u'Внутренняя ошибка'),
        (2, u'Ошибка разбора входных данных'),
        (3, u'Подпись не верна'),
        (4, u'Не найден сертификат подписи'),
        (5, u'Сертификат просрочен (один и/или несколько в цепочке)'),
        (6, u'Задано более одного сертификата (не используется)'),
        (7, u'Сертификат отозван (один и/или несколько в цепочке)'),
        (8, u'Не удалось найти подписи'),
        (9, u'Сообщение не подписано'),
        (10, u'Неверная конфигурация сервиса'),
        (11, u'Не удалось построить цепочку'),
        (13, u'Сертификат подписи не валиден'),
        (14, u'Не удалось выполнить проверку на отзыв '
             u'сертификата (одного и/или нескольких в цепочке)'),
        (15, u'Сертификат выдан центром сертификации, не входящим в ЕПД'),
    ]

    verify_signature_only = False
    service_url = api_settings.SMEV_VERIFIER_SERVICE_URL
    ESV_NAMESPACE = 'http://esv.server.rt.ru'

    def _get_method_name(self, signature):
        res = None
        if signature.code == CADES_X_LONG_TYPE_1:
            res = 'VerifyCAdESWithReport'

        assert res, u"Method can not be empty!"
        return res

    def _handle_response(self, response):
        root = etree.fromstring(response.content)
        code = root.findtext('.//{*}Code')
        description = root.findtext('.//{*}Description')
        try:
            code = int(code)
        except (ValueError, TypeError):
            raise AssertionError(u'Ошибка согласования протокола!')

        if code != self.SUCCESS_CODE:
            raise exceptions.VerificationError(
                description or self.RESPONSE_CODES[code][1]
            )

    @classmethod
    def is_suitable(cls, signature):
        suitable = (
            signature.code in cls._SUITABLE_SIGNATURE_TYPES and
            not signature.is_detached
        )
        return suitable

    def verify(self, signature):
        super(SMEVVerifier, self).verify(signature)

        message = signature.data.encode('base64')

        origin_content = None
        if signature.is_detached:
            origin_content = signature.origin_content.encode('base64')

        method_name = self._get_method_name(signature)
        render_params = {
            'verify_method_name': method_name,
            'message': message,
            'verify_signature_only': self.verify_signature_only,
            'origin_content': origin_content,
        }
        xml = _template.render(Context(render_params))

        headers = {
            'SOAPAction': '"%s/%s"' % (self.ESV_NAMESPACE, method_name),
            'Content-Type': 'text/xml;charset=UTF-8',
            'Accept-Encoding': 'gzip,deflate',
        }
        try:
            response = requests.post(
                self.service_url, data=xml, headers=headers
            )
        except requests.ConnectionError:
            raise exceptions.VerificationImpossibleError(
                u'Отсутствует связь с сервисом проверки подписи.'
            )
        else:
            if response.ok:
                self._handle_response(response)
            else:
                raise exceptions.VerificationImpossibleError(
                    u'Ошибка внешнего сервиса проверки подписи.'
                )
