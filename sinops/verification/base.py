# coding: utf8
import abc

from sinops import exceptions


class VerifierBase(object):

    u"""
    Базовый класс для проверки ЭЦП.

    Наследники должны реализовывать метод verify
    """

    __metaclass__ = abc.ABCMeta

    @classmethod
    def is_suitable(cls, signature):
        u"""
        Должен определять может ли данный бэкенд верифицировать
        подпись `signature`.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def verify(self, signature):
        u"""
        Метод должен проверять ЭЦП сообщения.

        В случае успешного выполнения должен возвращать None, иначе
        выбрасывать исключение VerificationError.
        """

        if not self.is_suitable(signature):
            raise exceptions.UnsuitableSignatureType
