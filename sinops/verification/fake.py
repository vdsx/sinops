# coding:utf8
from .base import VerifierBase
from sinops.exceptions import VerificationError


class AlwaysSuccessVerifier(VerifierBase):

    u"""Бэкенд проверки ЭЦП, считающий любую подпись верной."""

    @classmethod
    def is_suitable(cls, signature):
        u"""Всегда совместима."""
        return True

    def verify(self, signature):
        u"""Всегда успешная проверка."""


class AlwaysFailVerifier(VerifierBase):

    u"""Бэкенд проверки ЭЦП, считающий любую подпись не верной."""

    VERIFY_ERROR_MSG = u'Подпись не верна'

    @classmethod
    def is_suitable(cls, signature):
        u"""Всегда совместима."""
        return True

    def verify(self, signature):
        u"""Всегда ошибка верификации."""
        raise VerificationError(self.VERIFY_ERROR_MSG)

