# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Operation'
        db.create_table('sinops_operation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operation_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sinops.OperationType'])),
            ('signed_message', self.gf('django.db.models.fields.TextField')()),
            ('detached_content', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('verbose_content', self.gf('django.db.models.fields.TextField')()),
            ('signing_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('certificate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sinops.Certificate'])),
            ('request_path', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, db_index=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, db_index=True, blank=True)),
        ))
        db.send_create_signal('sinops', ['Operation'])

        # Adding model 'OperationType'
        db.create_table('sinops_operationtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100, db_index=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('sinops', ['OperationType'])

        # Adding model 'Certificate'
        db.create_table('sinops_certificate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('sha1hash', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40, db_index=True)),
            ('subject_name', self.gf('django.db.models.fields.TextField')(db_index=True)),
            ('issuer_name', self.gf('django.db.models.fields.TextField')(db_index=True)),
            ('serial_number', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('validity_begin', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('validity_end', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
        ))
        db.send_create_signal('sinops', ['Certificate'])


    def backwards(self, orm):
        # Deleting model 'Operation'
        db.delete_table('sinops_operation')

        # Deleting model 'OperationType'
        db.delete_table('sinops_operationtype')

        # Deleting model 'Certificate'
        db.delete_table('sinops_certificate')


    models = {
        'sinops.certificate': {
            'Meta': {'object_name': 'Certificate'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuer_name': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'serial_number': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'sha1hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40', 'db_index': 'True'}),
            'subject_name': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'validity_begin': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'validity_end': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        'sinops.operation': {
            'Meta': {'object_name': 'Operation'},
            'certificate': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sinops.Certificate']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'detached_content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'operation_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sinops.OperationType']"}),
            'request_path': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'signed_message': ('django.db.models.fields.TextField', [], {}),
            'signing_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'verbose_content': ('django.db.models.fields.TextField', [], {})
        },
        'sinops.operationtype': {
            'Meta': {'object_name': 'OperationType'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'db_index': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['sinops']
