=========================
Sinops
=========================

Sinops (signed operations) - пакет для создания в django приложении
представлений, допускающих исполнение, только при наличии специального
документа подписанного с использованием ЭЦП.
