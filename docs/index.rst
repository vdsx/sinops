.. Sinops documentation master file, created by
   sphinx-quickstart on Wed Sep 16 13:16:01 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документации пакета Sinops!
===========================



.. toctree::
   :maxdepth: 2

   tutorial.rst
   readme.rst



.. automodule:: sinops
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

