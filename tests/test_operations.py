# coding:utf8
import json
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import unittest
from django.test.client import Client

from sinops.settings import api_settings
from example.interface import SimpleTestInterface
from example import views as example_views

from .utils import load_file


class BaseSignedOpTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if cls is BaseSignedOpTest:
            raise unittest.SkipTest(
                "Skip BaseSignedOpTest tests, it's a base class")
        super(BaseSignedOpTest, cls).setUpClass()

    def setUp(self):
        self.client = Client()

    def _post_signature(self, view, extra_data=None):
        signature = load_file('cadesxlt1-signature1.b64')
        data = {api_settings.SIGNATURE_PARAM_NAME: signature}
        data.update(extra_data or {})
        return self.client.post(reverse(view), data)


class TestSignedOperation(BaseSignedOpTest):

    def test_ask_signature(self):
        response = self.client.post(reverse(example_views.so_fixmsg_view))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.ASK_SIGNATURE)
        self.assertEqual(response.context['content_to_sign'],
                         example_views.TEST_MESSAGE)

    def test_operation_completed(self):
        response = self._post_signature(
            example_views.so_fake_success_fixmsg_view)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.OPERATION_COMPLETED)

        result_value = response.context['result_value']
        self.assertIsInstance(result_value, HttpResponse)
        self.assertEqual(result_value.status_code, 200)
        self.assertEqual(result_value.content, 'success!')

    @unittest.SkipTest
    def test_verify_error(self):
        response = self._post_signature(example_views.so_fixmsg_view)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.VERIFY_ERROR)
        self.assertEqual(response.context['result_value'],
                         u'Электронная подпись неверна')

    def test_operation_error(self):
        response = self._post_signature(
            example_views.so_fake_success_raising_fixmsg_view)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.OPERATION_ERROR)

    def test_test_verify_error_with_dynamic_msg(self):
        response = self._post_signature(example_views.so_dynmsg_view)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.VERIFY_ERROR)

    def test_invalid_signature(self):
        response = self._post_signature(example_views.so_fixmsg_view, {
            api_settings.CONTEXT_PARAM_NAME: json.dumps({
                'signature_code': 'invalid_code'
            })
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.VERIFY_ERROR)
        self.assertEqual(
            response.context['result_value'],
            u'Тип ЭЦП "invalid_code" - недопустим'
        )


class TestStatefulSignedOperation(BaseSignedOpTest):

    def test_ask_signature(self):
        response = self.client.post(reverse(example_views.sso_fixmsg_view))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.ASK_SIGNATURE)
        self.assertIn('sinops', (k[:6] for k in self.client.session.keys()))

    def test_verify_error_incorrect_message(self):
        response = self._post_signature(example_views.sso_fixmsg_view)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result_type'],
                         SimpleTestInterface.VERIFY_ERROR)

    def test_operation_completed(self):
        ask_response = self.client.post(
            reverse(example_views.sso_fake_success_fixmsg_view))
        self.assertEqual(ask_response.context['result_type'],
                         SimpleTestInterface.ASK_SIGNATURE)
        op_response = self._post_signature(
            example_views.sso_fake_success_fixmsg_view)
        self.assertEqual(op_response.status_code, 200)
        self.assertEqual(op_response.context['result_type'],
                         SimpleTestInterface.OPERATION_COMPLETED)
        result_value = op_response.context['result_value']
        self.assertEqual(result_value.content, 'success!')
