# coding: utf8
u"""TODO."""
import os
import unittest

# Импорты для возможности запуска тестов по отдельности
from .test_operations import TestStatefulSignedOperation
from .test_operations import TestSignedOperation
from .test_signature import TestCAdESBESSignature
from .test_signature import TestCAdESXLT1Signature


def suite():
    """Find test cases."""
    BASE_DIR = os.path.dirname(__file__)
    test_cases = unittest.TestLoader().discover(
        'tests',
        pattern="test_*.py",
        top_level_dir=os.path.dirname(BASE_DIR),
    )
    return test_cases




