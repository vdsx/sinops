# coding:utf8
import os
from django.utils.functional import memoize


def _load_file(filename):
    _file = os.path.join(os.path.dirname(__file__), 'files', filename)

    with open(_file, 'rb') as f:
        return f.read()

load_file = memoize(_load_file, {}, 1)
