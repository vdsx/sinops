# coding: utf8
from django.utils import unittest

from sinops.signature.cert import X509Certificate
from sinops.signature.cades import CAdESXLT1, CAdESBES

from .utils import load_file


class TestX509Certificate(unittest.TestCase):

    def setUp(self):
        try:
            self.cert = X509Certificate(
                load_file('x509-cert1.der'))
        except:
            self.fail('Can not instantiate X509Certificate object')

    def test_attributes(self):
        self.assertEqual(self.cert.version, 3)
        self.assertEqual(
            self.cert.serial_number,
            int("1200076C1904A6E68B78CA53D6000000076C19", 16)
        )

    def test_fingerprint(self):
        self.assertEqual(
            self.cert.get_fingerprint('sha1'),
            'a536677fda8945d6e7277916c206eafabb164dfa'
        )
        self.assertEqual(
            self.cert.get_fingerprint('md5'),
            '63c6a8d17f76cc60d773509fe45cfe7f'
        )

    def test_invalid_cert(self):
        with self.assertRaises(ValueError):
            X509Certificate('bad data')

    def test_as_pem(self):
        pem = self.cert.as_pem()
        self.assertTrue(pem.startswith('-----BEGIN CERTIFICATE-----'))
        self.assertTrue(pem.endswith('-----END CERTIFICATE-----'))


class TestCAdESBESSignature(unittest.TestCase):

    CONTENT_VALUE = u'Тестовое сообщение - CADES_BES'
    SIGNATURE_CERTIFICATE_SERIAL = (
        401416781522226922897060870897293949309412803)

    def setUp(self):
        try:
            self.sig = CAdESBES(
                load_file('cadesbes-signature1.b64'), None,
                {'content_ecnoding': 'utf-16-le'})
        except:
            self.fail('Can not instantiate CAdESBES object')

    def test_origin_content(self):
        self.assertEqual(self.sig.origin_content,
                         self.CONTENT_VALUE.encode('utf-16-le'))

    def test_verbose_content(self):
        self.assertEqual(self.sig.verbose_content,
                         self.CONTENT_VALUE)

    def test_signature_cert_serial(self):
        cert = self.sig.get_certificate()
        self.assertEqual(
            cert.serial_number, self.SIGNATURE_CERTIFICATE_SERIAL)


class TestCAdESXLT1Signature(TestCAdESBESSignature):

    CONTENT_VALUE = u'Тестовое сообщение - Ура Python!'
    SIGNATURE_CERTIFICATE_SERIAL = (
        401415939236644982214727475513146224197200921)

    def setUp(self):
        try:
            self.sig = CAdESXLT1(
                load_file('cadesxlt1-signature1.b64'), None,
                {'content_ecnoding': 'utf-16-le'})
        except:
            self.fail('Can not instantiate CAdESXLT1 object')
